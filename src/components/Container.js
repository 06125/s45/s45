import React from "react";
import { Link } from "react-router-dom";

import { Jumbotron, Col, Row } from "react-bootstrap";

export default function Container() {
  return (
    //   React-Bootstrap Version

    <Row>
      <Col className="px-0">
        <Jumbotron>
          <h1 className="text-center mb-5">ERROR 404</h1>
          <h2 className="text-center ">CONGRATULATION.</h2>
          <h3 className="text-center mb-5">You broke the internet.</h3>
          <p className="text-center mb-5">
            <Link to="/">Back to Home</Link>
          </p>
        </Jumbotron>
      </Col>
    </Row>
  );
}
