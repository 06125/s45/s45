import React, { Fragment, useContext } from "react";
import { NavLink, Link, useHistory } from "react-router-dom";

// React Boostrap
import { Navbar, Nav } from "react-bootstrap";

import UserContext from "./../UserContext";

/* App navbar */
export default function AppNavbar() {
  const { user, unsetUser } = useContext(UserContext);

  // useHistory is a react-router-dom hook
  const history = useHistory();

  const logout = () => {
    unsetUser();
    history.push("/login");
  };

  let leftNav = !user.email ? (
    <Fragment>
      {/* prettier-ignore */}
      <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      {/* prettier-ignore */}
      <Nav.Link as={NavLink} to="/login">Log In</Nav.Link>
    </Fragment>
  ) : (
    <Fragment>
      {/* prettier-ignore */}
      <Nav.Link onClick={logout}>Logout</Nav.Link>
    </Fragment>
  );

  return (
    <Navbar bg="info" expand="lg">
      <Navbar.Brand as={Link} to="/">
        Course Booking
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {/* prettier-ignore */}
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          {/* prettier-ignore */}
          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
          <Nav>{leftNav}</Nav>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
