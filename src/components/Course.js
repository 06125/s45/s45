import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { Button, Card } from "react-bootstrap";

export default function Course({ course }) {
  console.log(course);

  // state
  let [count, setCount] = useState(0);
  let [seats, setSeats] = useState(10);
  let [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    if (seats === 0) {
      setIsDisabled(true);
    }
  }, [seats]);

  function enroll() {
    if (seats > 0) {
      setCount(count + 1);
      setSeats(seats - 1);
    }
  }

  return (
    <Card className="mb-3 col-6 offset-3">
      <Card.Body>
        <Card.Title>{course.name}</Card.Title>
        <h5 className="muted">Description</h5>
        <p>{course.description}</p>
        <h5 className="mute">Price</h5>
        <p>PhP {course.price}</p>
        <h5 className="mute">Enrollees</h5>
        <p>{count} Enrollees</p>
        <h5 className="mute">Seats</h5>
        <p>{seats} seats</p>
        <Button
          variant="primary"
          onClick={
            enroll
            // () => {count < 30 ? setCount(count + 1) : alert("No slot available")}
          }
          disabled={isDisabled}
        >
          Enroll
        </Button>
      </Card.Body>
    </Card>
  );
}

// Some form of validation, just like JOI in mongoose
Course.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
