import React from "react";
import { Col, Row, Card, Button } from "react-bootstrap";

export default function CourseCard() {
  return (
    <Row className="justify-content-center my-5">
      <Col xs={10} md={6}>
        <Card>
          <Card.Body>
            <Card.Title>Sample Course</Card.Title>
            <Card.Text>
              <Card.Subtitle className="mb-2 text-muted">
                Description
              </Card.Subtitle>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </Card.Text>
            <Card.Text>
              <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
              PhP 40,000
            </Card.Text>
            <Button variant="primary">Enroll</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
