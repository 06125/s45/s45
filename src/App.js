import React, { useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import UserContext from "./UserContext";

import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
// import CourseCard from "./components/CourseCard";
// import Welcome from "./components/Welcome";
// import Counter from "./components/Counter";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Container from "./components/Container";

export default function App() {
  // const [user, setUser] = useState(null);
  const [user, setUser] = useState({ email: localStorage.getItem("email") });
  const unsetUser = () => {
    localStorage.clear();
    setUser({ email: null });
  };

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <BrowserRouter>
        <AppNavbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/courses" component={Courses} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/login" component={Login} />
          <Route exact path="*" component={Container} />
        </Switch>
      </BrowserRouter>
    </UserContext.Provider>
  );
}

//  <Fragment>
// <AppNavbar />
// <Home />
// <Courses />
// <Register />
// <Login />
//  <CourseCard />
//  <Welcome name="John Paul" />
//  <Welcome name="Rayvin" />
//  <Counter />
// </Fragment>;
