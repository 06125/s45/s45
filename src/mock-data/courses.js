const data = [
  {
    id: "wdc001",
    name: "PHP-Laravel",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, deserunt. Incidunt inventore animi magni nihil natus facere et amet voluptatum ex fuga. Optio dolore itaque, nostrum consequatur sed saepe ratione.",
    price: 45_000,
    onOffer: true,
  },
  {
    id: "wdc002",
    name: "Python-Django",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, deserunt. Incidunt inventore animi magni nihil natus facere et amet voluptatum ex fuga. Optio dolore itaque, nostrum consequatur sed saepe ratione.",
    price: 50_000,
    onOffer: true,
  },
  {
    id: "wdc003",
    name: "Java-Springboot",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, deserunt. Incidunt inventore animi magni nihil natus facere et amet voluptatum ex fuga. Optio dolore itaque, nostrum consequatur sed saepe ratione.",
    price: 55_000,
    onOffer: true,
  },
];

export default data;
