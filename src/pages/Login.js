import React, { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { Redirect } from "react-router-dom";

import UserContext from "./../UserContext";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isDisabled, setIsDisabled] = useState(true);

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (email && password) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password]);

  const login = (e) => {
    e.preventDefault();

    setUser({ email });
    localStorage.setItem("email", email);
    alert("Logged In Successfully.");
    setEmail("");
    setPassword("");
    setIsDisabled(true);
  };

  if (user.email) {
    return <Redirect to="/"></Redirect>;
  }

  return (
    <Container className="mb-5">
      <Row>
        <Col>
          <h1 className="text-center">Log In</h1>
          <Form onSubmit={login}>
            <Form.Group controlId="formEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="formPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={isDisabled}>
              Login
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
