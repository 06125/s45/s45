import React from "react";
import { Container } from "react-bootstrap";

import Banner from "./../components/Banner";
import Highlights from "./../components/Highlights";

export default function Home() {
  return (
    <Container fluid className="px-0">
      <Banner />
      <Highlights />
    </Container>
  );
}
